package presentation.stylesheets

import javafx.scene.layout.BorderStrokeStyle
import javafx.scene.paint.Color
import tornadofx.*

class Styles : Stylesheet() {
    init {
        textField {
            borderStyle += BorderStrokeStyle.SOLID
            borderWidth += box(2.px)
            prefWidth = 200.px
        }
        menuButton {
            borderStyle += BorderStrokeStyle.SOLID
            backgroundColor += Color.WHITE
            borderWidth += box(2.px)
            prefWidth = 200.px
        }
        button {
            backgroundColor += Color.BLACK
            textFill = Color.WHITE
        }
    }
}