package presentation.main

import domain.interactors.*
import javafx.beans.property.*
import javafx.scene.control.*
import javafx.scene.layout.*
import jiconfont.icons.FontAwesome
import jiconfont.javafx.*
import khttp.get
import presentation.push.PushView
import server
import tap
import tornadofx.*


class MainView : View() {
    override val root: AnchorPane by fxml()
    private val gameLocationField: TextField by fxid()
    private val idField: TextField by fxid()
    private val realmField: MenuButton by fxid()
    private val characterField: MenuButton by fxid()

    private val gameLocationBorderPane: BorderPane by fxid()
    private val idBorderPane: BorderPane by fxid()

    private val submitButton: Button by fxid()
    private val buttonEnabled: BooleanProperty = SimpleBooleanProperty(true)

    init {
        val gameLocation = GetGameLocation.perform() ?: ""
        gameLocationField.text = gameLocation

        submitButton.enableWhen { SimpleBooleanProperty(true) }
        submitButton.setOnMouseClicked {
            PushView.id = idField.text
            replaceWith<PushView>(ViewTransition.Slide(0.3.seconds, ViewTransition.Direction.LEFT))
        }

        populateGameFields(gameLocation)
        populateIDField("")

        idField.textProperty().addListener { _, _, new -> populateIDField(new) }
        gameLocationField.textProperty().addListener { _, _, new -> populateGameFields(new) }
    }

    // Todo: Turn the beginning of these methods into a new abstraction
    private fun populateIDField(id: String) {
        if (id != "") {
            println("$server/$id/exists")
            val x = get("$server/$id/exists")
            println(x)
            val validGroup = x.jsonObject.getString("exists") == "true"
            val icon = if (validGroup) FontAwesome.CHECK else FontAwesome.TIMES

            idBorderPane.right = button {
                addClass("icon-only")
                style = "-fx-background-color: #F4F4F4;"
                graphic = IconNode(icon).apply { iconSize = 24 }
            }
        }
    }

    private fun populateGameFields(gameLocation: String) {
        val validLocation = GameLocationValidator.perform(gameLocation)
        val icon = if (validLocation) FontAwesome.CHECK else FontAwesome.TIMES

        gameLocationBorderPane.right = button {
            addClass("icon-only")
            style = "-fx-background-color: #F4F4F4;"
            graphic = IconNode(icon).apply { iconSize = 24 }
        }



        val charactersByRealm = CreateCharacters.perform(gameLocation).groupBy { it.realm }
        val selectedRealm = charactersByRealm.keys.takeIf { it.isNotEmpty() }?.first()
        val selectedCharacter = charactersByRealm[selectedRealm]?.get(0)

        realmField.items.setAll(charactersByRealm.keys.map { realm ->
            MenuItem(realm).tap { menuItem ->
                menuItem.action {
                    if (realmField.text != realm)
                        characterField.text = ""
                    realmField.text = realm
                }
            }
        })

        characterField.items.setAll(charactersByRealm[selectedRealm]?.map { character ->
            MenuItem(character.name).tap { menuItem ->
                menuItem.action {
                    characterField.text = character.name
                    PushView.character = character
                }
            }
        } ?: listOf())


        realmField.text = selectedRealm ?: "No Realm Found"
        characterField.text = selectedCharacter?.name ?: "No Characters Found"
        PushView.character = selectedCharacter

        buttonEnabled.value = validLocation && selectedRealm != null && selectedCharacter != null
    }
}