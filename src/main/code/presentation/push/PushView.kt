package presentation.push

import domain.entities.PlayerCharacter
import domain.interactors.KeyboardListener
import javafx.scene.control.*
import javafx.scene.layout.*
import mainStage
import tornadofx.*
import khttp.*
import org.json.JSONObject
import propertiesMap
import server

class PushView: View() {
    companion object {
        var character: PlayerCharacter? = null
        var id: String? = null
    }

    override val root: AnchorPane by fxml()
    private val characterNameLabel: Label by fxid()
    private val progress: ProgressIndicator by fxid()

    init {
        characterNameLabel.text = PushView.character?.name ?: "Character not submitted."
        val character: PlayerCharacter? = PushView.character
        val id: String? = PushView.id

        if (character != null && id != null)
            main(character, id)
        else
            progress.hide()
    }

    private fun main(character: PlayerCharacter, id: String) {
        progress.progress = ProgressBar.INDETERMINATE_PROGRESS


        post("$server/$id/players", data=mapOf("nameRealm" to character.nameRealm))
        character.spells.forEach { spell -> post("$server/$id/spells", data=spell.propertiesMap) }

        val thread = Thread { KeyboardListener.perform { keyWithModifier ->
            val spellsByKeybind = character.spells.groupBy { it.keybinding }
            val spell = spellsByKeybind[keyWithModifier]?.get(0)

            if (spell != null) {
                spell.javaClass.declaredFields.map { it.name }
                post("$server/$id/spellTriggered",  data=spell.propertiesMap)
            }
        }}
        thread.start()

        mainStage.setOnCloseRequest {
            println("thread stopped")
            thread.stop()
        }
    }
}