import javafx.stage.Stage
import jiconfont.icons.FontAwesome
import jiconfont.javafx.IconFontFX
import presentation.main.MainView
import presentation.stylesheets.Styles
import tornadofx.*

const val addonName = "HelloWorld"
val server = "http://wow-party-cooldown-tracker.herokuapp.com/groups"
val x = "http://localhost:7000/groups"

lateinit var mainStage: Stage

object Main {
    @JvmStatic fun main(args: Array<String>) {
        IconFontFX.register(FontAwesome.getIconFont())
        launch<GUI>()
    }

    class GUI : App(MainView::class, Styles::class) {
        override fun start(stage: Stage) {
            super.start(stage)
            mainStage = stage
        }
    }
}

