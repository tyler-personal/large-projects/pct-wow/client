package domain.interactors

import domain.entities.PlayerCharacter
import java.io.File

object CreateCharacters {
    fun perform(gameLocation: String): List<PlayerCharacter> {
        val location = "$gameLocation\\WTF\\Account"

        fun getDirNames(location: String): List<String> =
            File(location)?.list { current, name -> File(current, name).isDirectory }?.toList() ?: listOf()

        return getDirNames(location).flatMap { account ->
            getDirNames("$location\\$account").flatMap { realm ->
                getDirNames("$location\\$account\\$realm").mapNotNull { character ->
                    val file = GetAddonFile.perform(account, realm, character, location)
                    if (file != null)
                        PlayerCharacter(account, realm, character, file)
                    else null
                }
            }
        }
    }
}