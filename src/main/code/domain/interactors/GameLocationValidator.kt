package domain.interactors

import java.io.File

object GameLocationValidator {
    fun perform(location: String) = File("$location//WTF//Account").exists()
}