package domain.interactors

import java.io.File

object GetGameLocation {
    fun perform(): String? {
        return File.listRoots().map(Any::toString).flatMap { root ->
            listOf("Program Files (x86)", "Program Files", "Games").map { "$root$it\\World of Warcraft"}
        }.find { File(it).exists() }
    }
}