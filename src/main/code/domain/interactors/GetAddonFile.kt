package domain.interactors

import addonName
import java.io.File

object GetAddonFile {
    fun perform(account: String, realm: String, character: String, gameLocation: String) =
        File("$gameLocation\\$account\\$realm\\$character\\SavedVariables\\$addonName.lua").takeIf { it.exists() }
}