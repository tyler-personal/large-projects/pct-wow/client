package domain.interactors

import lc.kra.system.keyboard.GlobalKeyboardHook
import lc.kra.system.keyboard.event.*

object KeyboardListener {
    fun perform(funcToExecuteOnKeyEventWithModifier: (String) -> Unit) {
        val keyboard = GlobalKeyboardHook(false)

        keyboard.addKeyListener(object: GlobalKeyAdapter() {
            override fun keyPressed(e: GlobalKeyEvent) {
                fun mod(bool: Boolean, str: String) = if (bool) str else ""

                val key: String = mod(e.isShiftPressed, "SHIFT-") + mod(e.isControlPressed, "CTRL-") +
                    mod(e.isMenuPressed, "ALT-") + e.keyChar.toUpperCase()

                if (e.keyChar.category != CharCategory.CONTROL) {
                    funcToExecuteOnKeyEventWithModifier(key)
                }
            }

            override fun keyReleased(event: GlobalKeyEvent) {
            }
        })
    }

}