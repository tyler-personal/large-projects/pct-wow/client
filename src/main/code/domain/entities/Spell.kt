package domain.entities

import org.json.JSONObject

class Spell(jObj: JSONObject, val nameRealm: String) {
    val name = jObj.getString("name")
    val id = jObj.getInt("id")
    val cooldown = jObj.getDouble("cooldown")
    val keybinding = jObj.getString("keybinding")
}