package domain.entities

import org.json.JSONObject
import java.io.File

class PlayerCharacter(val account: String, val realm: String, val name: String, addonFile: File) {
    val nameRealm = "$name-$realm"
    val spells: List<Spell> = addonFile.readLines().mapNotNull { line ->
        if (line.contains("json = \"")) {
            val json = line.replace("json = \"", "").replace("\\", "").substringBeforeLast("\"") + "]}"
            val jArr = JSONObject(json).getJSONArray("spells")
            jArr.map { Spell(JSONObject(it.toString()), nameRealm) }
        } else null
    }.flatten()
}