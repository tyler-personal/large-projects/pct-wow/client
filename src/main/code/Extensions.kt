import kotlin.reflect.full.memberProperties

fun <T> T.tap(func: (T) -> Any): T { func(this); return this; }

inline val <reified T: Any> T.propertiesMap: Map<String, String>
    get() = T::class.memberProperties.map { it.name to it.getter.call(this).toString() }.toMap()